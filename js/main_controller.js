/**
 * Created by rafaelbattesti (991382266) on 2016-05-19.
 */

//Function declaration - parseXML
function parseXML(xml) {

    var imgFolder = "img/";
    var pagesFolder = "pages/";

    $(xml).find("student").each(function () {
        $("#studentName").html($(this).find("studentName"));
        $("#studentNumber").html($(this).attr("studentNumber"));
        $("#studentProg").html($(this).find("studentProg"));
        $("#studentCampus").html($(this).find("studentCampus"));
        $("#image").attr("src", imgFolder + $(this).find("image").text());
    });

    $(xml).find("movie").each(function () {

        var name = $(this).attr("movieName");
        var moviePath = pagesFolder + name + ".html";
        var img = imgFolder + $(this).find("moviePoster").text();
        var container = $("<div class='col-xs-4'>").append(
            $("<a id='" + name + "' href='" + moviePath + "'>").append(
                $("<img class='img-circle' src='" + img + "'>")));

        $("#movies").append(container);
    });
}

//Actions take place after the DOM is loaded
$(document).ready(function () {

    //AJAX call for the xml content for main page
    $.ajax({
        type: "GET",
        url: "a1.xml",
        dataType: "xml",
        success: parseXML
    });
});