/**
 * Created by rafaelbattesti (991382266) on 2016-06-03.
 */


//Function declaration - parseXML
function parseXML(xml) {

    var element;
    var title;
    var imgPath;

    //Routes to the correct page
    if ($(location).attr('href').indexOf("forest_gump") > -1) {

        element = $(xml).find("movie[movieName='forest_gump']");
        title = formatTitle(element.attr("movieName"));
        imgPath = "../img/";

    } else if ($(location).attr('href').indexOf("the_godfather") > -1) {

        element = $(xml).find("movie[movieName='the_godfather']");
        title = formatTitle(element.attr("movieName"));
        imgPath = "../img/";

    } else if ($(location).attr('href').indexOf("the_return_of_the_jedi") > -1) {

        element = $(xml).find("movie[movieName='the_return_of_the_jedi']");
        title = formatTitle(element.attr("movieName"));
        imgPath = "../img/";
    }

    //Sets the title of the page from xml
    $("title").html(title);
    $("img").attr("src", imgPath + element.find("moviePoster").text());
    $("#movieName").html(title);
    $("#movieDirector").html(element.find("movieDirector"));
}

//Capitalizes the name of the movie from xml movieName attribute (used as url)
function formatTitle (movieName) {
    var splitName = movieName.split("_");
    var newArray = [];

    for (var i = 0; i < splitName.length; i++) {
        newArray[i] = splitName[i].charAt(0).toUpperCase();
        newArray[i] += splitName[i].substr(1);
    }

    return newArray.join(" ");
}


//Actions take place after the DOM is loaded
$(document).ready(function () {

    //AJAX call for the xml content for main page
    $.ajax({
        type: "GET",
        url: "../a1.xml",
        dataType: "xml",
        success: parseXML
    });
});